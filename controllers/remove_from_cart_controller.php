<?php 	
	
	//invoke session
	session_start();

	//catch the product id
	$id = $_GET['id'];

	//destroy na product inside the session cart
	//unset to destroy session in cart or data
	unset($_SESSION['cart'][$id]);

	//create a control structure that will check whenever the items saved on the CART Session will be 0.. then the cart will be destroyed
	if (count($_SESSION['cart'] === 0)) {
		# code...
		unset($_SESSION['cart']);
	}

	header("location: {$_SERVER['HTTP_REFERER']} ");


?>