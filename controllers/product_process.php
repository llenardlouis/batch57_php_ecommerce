<?php 

	require_once 'connection.php';
	session_start();

	$product_name = $_POST['product-name'];
	$product_price = $_POST['product-price'];
	$product_description = $_POST['product-description'];
	$product_category = $_POST['product-category'];

	// //photo upload
 //    $image = $_FILES['product-image'];
 //    $image_name = $image['name'];
 //    $image_type = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));  
 //    $image_tmpname = $image['tmp_name'];
 //    $image_size = $_FILES['product-image']['size']; 

 //    //validation to check the file extension name

 //    $is_not_image = true;
 //    $is_empty_file = true;

	// //validaton to set the limit of the  file size.
 //    if( $image_size > 0 && $image_size < 800000 ){

 //      $is_empty_file = false;
 //    };

 //    if($image_type =="jpg" || $image_type == "jpeg" || $image_type == "png" || $image_type = "gif" || $image_type == "svg") {
 //        $is_not_image = false;
 //    };

 //    //validation for files with similar file names

 //    if (!$is_not_image && !$is_empty_file) {
 //      # code...
 //      $image_destination = "./../assets/images/".date("Y-m-d-h-i-s",time())." $image_name";

 //      move_uploaded_file($image_tmpname, $image_destination);
      
 //      if (strlen($product_name) == 0 || strlen($product_price) == 0 || strlen($product_description) == 0 || strlen ($product_category) == 0  ) {
	// 		header ("location: ./../views/add_product.php");
	
	// 	}else{

	// 		$sql_query = "INSERT INTO products(name,price,description,image,category_id) VALUES ('$product_name','$product_price','$product_description','$image_name','$product_category')";

	// 		$result = mysqli_query($conn, $sql_query);
	// 		// header ("location: ./../views/add_product.php");
	// 		echo "Successfully Added a product";
	// 	}

 //    }else {
 //      echo "<h2> Error Upload </h2>";
 //    }

    //sir marty
	//List of validation that im going to use with their default values

	$incomplete_fields = true;
	$is_not_image = true;
    $is_empty_file = true;

    if(empty($product_name) || empty($product_price) || empty($product_description) || empty($product_category) || !is_numeric($product_price) )
    {
    	$incomplete_fields = true;
    }else{
    	$incomplete_fields = false;
    }

    $product_image = $_FILES['product-image'];
    $product_image_name = $product_image ['name'];
    $product_image_size = $product_image ['size'];
    $product_image_type = strtolower(pathinfo($product_image_name, PATHINFO_EXTENSION));
    $product_image_tmpname = $product_image['tmp_name'];
    $product_image_path = date("Y-m-d-h-i-s",time()).$product_image_name;

    //validation to check if file is empty and also to set a LIMIT
    if($product_image_size > 0 && $product_image_size < 800000 ){

    	echo "Not an empty file";
    	$is_empty_file = false;
    }
    //validation to check the file format (JPEG, JPG, PNG, GIF, SVG)
    if($product_image_type == "jpg" || $product_image_type == "png" || $product_image_type == "jpeg" || $product_image_type == "gif" || $product_image_type == "svg" ){

    	echo "Uploaded file is an image file!";
    	$is_not_image = false;
    }

    //to insert data inside the database if all requirements are valid

    if(!$incomplete_fields && !$is_empty_file && !$is_not_image){

    	$product_image_destination = "./../assets/images/" .$product_image_path;
    	move_uploaded_file($product_image_tmpname, $product_image_destination);

    	//insert query to insert data in the database
    	$cat_insert = "INSERT INTO products(name,price,description,image,category_id) VALUES ('$product_name','$product_price','$product_description','$product_image_path','$product_category')";

    	mysqli_query($conn,$cat_insert);
    	// echo "Your file is successfully uploaded!";
    	header('location: ./../views/add_product.php');
    }else{
    	echo "Error in uploading data";

    	$_SESSION['error message'] = "All fields are required!";
    	header('location: ./../views/add_product.php');


    }

        
 ?>    
