<?php 
	
	session_start();

	function getCartCount(){
		return array_sum($_SESSION['cart']);
	}

	$product_id = $_POST['productId'];
	$product_quantity = $_POST['productQuantity'];

	if(isset($_SESSION['cart'][$product_id])){

		$_SESSION['cart'][$product_id] += $product_quantity;

	}else {
		//we will declare a session variable named cart with a key equal to received product id from catalog fetch request and value equal to received quantity

		$_SESSION['cart'][$product_id] = $product_quantity;
	} 
	// echo getCartCount();	
 ?>	