<?php

	// REQUIRE PHPmailer
	// Import PHPMailer classes into the global namespace
	// These must be at the top of your script, not inside a function
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;

	// Load Composer's autoloader
	require './../vendor/autoload.php';

	//end php mailer require
// ======================

	//invoke session
	session_start();

	//create a control structure that will make sure that the user is login when making a transaction
	if(!isset($_SESSION['user'])){
		//if the session is empty then browser redirect to login so the user can login first
		header("location: ./../views/login.php");
	}

	$total = 0;
	$product_id = join(",",array_keys($_SESSION['cart']));
	// invoke the database
	require_once "./connection.php";

	//create a query to get all the products from the database
	$sql_query_get_cart = "SELECT * FROM products WHERE id IN ($product_id) ";
	$result = mysqli_query($conn, $sql_query_get_cart);

	while($product = mysqli_fetch_assoc($result)){
		$subtotal = $product['price'] * $_SESSION['cart'][$product['id']];
		$total += $subtotal;
	}

	//get the detail of the customer and details of the transactions
	$transaction_code = generateTransactionCode();
	$user_id = $_SESSION['user']['id'];
	//catch the type of the payment mode from cart.php
	$payment_mode_id = $_POST['payment-mode'];
	$status_id = 1;

	function generateTransactionCode(){
		$transaction_code = "";
		$chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];

		for($i=0;$i < 5; $i++ ){
			$index = rand(0,15);

			$transaction_code = $transaction_code . $chars[$index];
		}

		$transaction_code = $transaction_code. getdate()[0];

		return $transaction_code;

	}

	//create a query to add the transaction details inside the  table
	$sql_query_add_transaction = "INSERT INTO transactions (transaction_code,total,user_id,status_id,payment_mode_id) VALUES ('$transaction_code',$total,$user_id,$status_id,$payment_mode_id) ";

	$result = mysqli_query($conn, $sql_query_add_transaction);

	$transaction_id = mysqli_insert_id($conn);

	$array_entries = [];
	foreach ($_SESSION['cart'] as $id => $quantity) {
		# code...
		$array_entries[] = "($transaction_id, $id, $quantity)";
	}

	$values = join(",",$array_entries);

	$query = "INSERT INTO product_transactions(transaction_id,product_id,quantity) VALUES $values ";

	mysqli_query($conn, $query);

	// ==============================
	// start of send email to customer
	// ====================


	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);

	try {
	    //Server settings
	    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
	    $mail->isSMTP();                                            // Send using SMTP
	    $mail->Host       = 'smtp1.gmail.com';                    // Set the SMTP server to send through
	    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	    $mail->Username   = 'phpmailert3st3r@gmail.com';                     // SMTP username
	    $mail->Password   = 'walongaskterisk';                               // SMTP password
	    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

	    //Recipients
	    $mail->setFrom('phpmailert3st3r@gmail.com', 'PushCart Corp');
	    $mail->addAddress('nafob85236@tgres24.com');               // Name is optional
	    // $mail->addReplyTo('info@example.com', 'Information');
	    // $mail->addCC('cc@example.com');
	    // $mail->addBCC('bcc@example.com');

	    // Attachments
	    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	    // Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Thank you for Buying';
	    $mail->Body    = "<h1> Your Transaction code is <strong>{$transaction_code}</strong></h1>";
	    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	    $mail->send();
	    echo 'Message has been sent';
	} catch (Exception $e) {
	    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}

	// end send email
	// =========================

	//at event of the clear cart
	unset($_SESSION['cart']);

	//browser redirect going to transactions.php
	// header("location: ./../views/transactions.php")


?>