<?php 
	// catch the product id
	$product_id = $_GET['id'];
	//get the data from the edit_product.php

	// productname
	$product_name = htmlspecialchars(trim($_POST['product-name']));
	//product price
	$product_price = htmlspecialchars(trim($_POST['product-price']));
	//product category
	$product_category = htmlspecialchars(trim($_POST['product-category']));
	//product description
	$product_description = htmlspecialchars(trim($_POST['product-description']));
	//now we have to validate the entered datas before saving in our database

	//check if file is selected for the image
	function checkIfFileSelected($file){
		if(empty($_FILES['product-image']['name'])){
			return false;
		}else{
			return true;
		}
	}
	//now create a function that will check the file type size
	function saveFile($file){

		$filename = $file['name'];
		$filesize = $file['size'];
		$filetype = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
		$file_tmp_name = $file['tmp_name'];
		$file_image_path = date("Y-m-d-h-i-s",time()).$filename;
	//validation names
	//validation that will check for the file type
		$is_not_image = true;
	//validation to check if the file is more than 0 but less than the limit
		$is_empty_file = true;

		if($filetype == "jpg" || $filetype == "jpeg" || $filetype == "png" || $filetype == "gif" || $filetype == "svg" ){
			$is_not_image = false;
		}
		if($filesize > 0 && $filesize < 800000){
			$is_empty_file = false;
		}

		if(!$is_empty_file && !$is_not_image){
			//destination for the new uploaded image
			// $destination = "./../assets/images/$filename";
			$destination = "./../assets/images/" .$file_image_path;
			move_uploaded_file($file_tmp_name, $destination);

			return $file_image_path;
		}
		return false;
	}

	//create a function to check if all inputs have data
	function checkInputIsComplete($name,$price,$category,$description){
		if(empty($name) || empty($price) || empty($description) || empty($category)){
			return false;
		}else {
			return true;
		}
	}

	if(checkInputIsComplete($product_name,$product_price,$product_category,$product_description)){

		require './../controllers/connection.php';
		if (checkIfFileSelected($_FILES['product-image'])) {
			# code...
			$image = saveFile($_FILES['product-image']);
			// $image = saveFile($file_image_path);
			//query to update file in the database

			$sql_update_product = "UPDATE products SET name = '{$product_name}', price={$product_price}, category_id = {$product_category}, description = '{$product_description}', image='$image' WHERE id = {$product_id} ";
			//connect to database and query
			// var_dump($sql_update_product);
			mysqli_query($conn,$sql_update_product);
			//browser redirect if update is successful
			// header("location: ./../views/catalog.php");
			header("location: ./../views/catalog.php?={$product_id}");

		}else{

			$sql_update_product = "UPDATE products SET name = '{$product_name}', price={$product_price}, category_id = {$product_category}, description = '{$product_description}' WHERE id = {$product_id} ";
			//connect to database and query
			// var_dump($sql_update_product);
			mysqli_query($conn,$sql_update_product);
			//browser redirect if update is successful
			// header("location: ./../views/catalog.php");
			header("location: ./../views/catalog.php?={$product_id}");
			//browser redirect if update is not successful			
		}
	}else{
		header("location: {$_SERVER['HTTP_REFERER']} ");
	}

 ?>

 <!-- 
 //trim()- strips white spaces(or other characters) from the beginning and end of a string 
htmlspecialchars() - converts special characters to HTML entities

 -->