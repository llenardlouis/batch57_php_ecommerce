<?php 
	
	//invoke the session cart
	session_start();

	//destroy the session
	unset($_SESSION['cart']);

	//browser redirect
	header("location: {$_SERVER['HTTP_REFERER']} ");

?>