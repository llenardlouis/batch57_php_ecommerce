<?php 
	
	//invoke session
	session_start();

	//catch the product id and put it inside a container
	$id = $_GET['id'];

	//catch the quantity and put it inside a container
	$qty = $_POST['quantity'];

	//save the new quantity currently stored in the SESSION cart.
	$_SESSION['cart'][$id] = $qty;

	//browser redirect to the previous page
	header("location: {$_SERVER['HTTP_REFERER']}");
?>