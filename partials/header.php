<nav class="navbar navbar-expand-lg navbar-dark main-nav sticky-top">
  <a class="navbar-brand" href="#">PushCart</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <div class="navbar-nav">
        <a class="nav-item nav-link active" href="./../views/home.php">Home <span class="sr-only">(current)</span></a>
        <a class="nav-item nav-link" href="./../views/add_category.php">Add Category</a>
        <a class="nav-item nav-link" href="./../views/add_product.php">Add Product</a>
        <a class="nav-item nav-link" href="./../views/catalog.php">Catalog</a>
        <a class="nav-item nav-link" href="./../views/cart.php">
          Cart
          <span class="badge" id="cart-count">
            <?php 
                if(isset($_SESSION['cart'])) {
                  echo array_sum($_SESSION['cart']);
                }else {
                  echo 0;
                }
             ?>
          </span>
        </a>
        
         <a class="nav-item nav-link" href="./../views/register.php" >Register</a>
        <?php 
            if(!isset($_SESSION['user'])){
              ?>
                  <a class="nav-item nav-link" href="./../views/login.php" >Login</a>
              <?php

            }
         ?>
        
        <?php 
            if(isset($_SESSION['user'])){
              ?>
                 <a class="nav-item nav-link" href="./../controllers/logout.php">Logout</a>
              <?php

            }
         ?>
        
    </div>
  </div>
</nav>
