<?php 
	require_once './../partials/template.php';

	function get_content(){
		?>	
			<h3 class="text-center mb-2">Add Category Form</h3>
			<div class="container">
				<div class="row my-2">
					<div class="col-12 col-sm-10 col-md-8 mx-auto">
						<!-- adding categories -->
						<form action="./../controllers/process_category.php" method="POST">
							<div class="form-group">
								<label for="catName">Category Name:</label>
								<input type="text" name="catName" id="catName" class="form-control">
							</div>
							
							<button type="submit" class="btn btn-primary">Add Category</button>
						</form>
						<hr>
						<div class="categoryList">
							<div class="table-responsive">
								<table class="table table-striped table-bordered" id="cart-items">
									<thead>
										<tr>
											<th class="categories-header" colspan="2">Categories</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<?php 
												require './../controllers/connection.php';

												$sql_query = "SELECT id, name from categories" ;

												$result = mysqli_query($conn,$sql_query);

												//retrieve or display all list in database
												while($row = mysqli_fetch_assoc($result)){ 
													?>
														<tr> 
															<td class="category-name"><?php  echo $row['name']  ?>
																
																	<td>
																<a href="./../controllers/edit_category_form.php?id=<?php echo $row['id'] ?>" class="btn btn-success">Edit Category</a>

																<a href="./../controllers/delete_category.php?id=<?php echo $row['id'] ?>" class="btn btn-danger">Delete Category</a>
																</td>
															
															</td>
														</tr>
													<?php
												};
											?>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		<?php
	}
 ?>