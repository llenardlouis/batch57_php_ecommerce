<?php 
	
	require_once './../controllers/connection.php';
	require_once './../partials/template.php';


	function get_content(){

		global $conn; 
		?>
		
			<div class="container my-4 cart-table">
				<div class="row">
					<div class="col-lg-12">
						<h2>Cart Page</h2>
					</div>
				</div>

				<hr>

				<div class="table-responsive">
					<table class="table table-striped table-bordered" id="cart-items">
						<thead>
							<tr>
								<th>Items</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<!-- name -->
							<!-- description -->
							<!-- price -->
							<?php 
								if( isset($_SESSION['cart']) && count($_SESSION['cart']) > 0 ){
									$total = 0;

									foreach ($_SESSION['cart'] as $product_id => $product_quantity) {
										# code... create a query that will insert from products table the product that matches the product id
										$sql_query = "SELECT * FROM products WHERE id = $product_id ";

										// var_dump($sql_query);
										$result = mysqli_query($conn,$sql_query);
										$indiv_product =  mysqli_fetch_assoc($result);
										//converts an associative array into a number of variable with names as the keys
										extract($indiv_product);
										//we have now the following variables:
											//id
											//name
											//description
											//image
											//price
											//category_id
										//creating formula for the subtotal
										$subtotal = $price * $product_quantity;
										$total += $subtotal; 
							?>
							<tr>
								<td><?=  $name ?></td>
								<td><?=  $price ?></td>
								<td><?=  $product_quantity ?></td>
								<td><?=  number_format($subtotal,2) ?></td>
								<td>
									<form action="./../controllers/replace_cart_controller.php?id=<?= $id ?>" method="POST">
										<label for="quantity">Edit Quantity</label>
										<input type="number" name="quantity" id="quantity" class="form-control form-control-sm" value="<?php echo $product_quantity ?>">
										<button class="btn btn-warning w-100 my-1">Change Qty.</button>
									</form>
									<!-- remove item from cart -->
									<a href="./../controllers/remove_from_cart_controller.php?id=<?= $id ?>" class="btn btn-danger w-100 my-1">Remove Item</a>	
								</td>
							</tr>
						<?php  } 
						?>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td> Total: <span id="total-amount"><?=  $total ?></span> </td>
								<td>
									<!-- payment modes -->
									<form action="./../controllers/create_transactions.php" method="POST">
										<div class="form-group">
											<label for="payment-mode">Choose Payment Method:</label>
											<select name="payment-mode" id="payment-mode" class="form-control">
												<?php
													$sql_query_get_payment_method = "SELECT * FROM payment_modes";
													$result = mysqli_query($conn,$sql_query_get_payment_method);

													while ($mode = mysqli_fetch_assoc($result)) {
														# code...
														?>
															<option value="<?php echo $mode['id']; ?>">
																<?php echo $mode['name']; ?>
															</option>
														<?php
													}

												?>
											</select>
											<button class="btn btn-success w-100 my-1">
												Checkout
											</button>
										</div>
									</form>

									<div class="text-center my-3"> OR</div>

									<div id="paypal-smart-button">
										
									</div>
									<!-- end of payment mode -->
									<!-- clear cart -->
									<a href="./../controllers/clear_cart_controller.php" class="btn btn-outline-danger">Clear Cart</a>
								</td>
							</tr>
							
						<?php } else {
								echo "<tr><td class='text-center' colspan='6' >No Products in Cart</td></tr>";
							}
						?>	
						</tbody>
					</table>
				</div>
			</div>
			
			<script
		    	src="https://www.paypal.com/sdk/js?client-id=AV-4IDLzXrN0smIiuhOPeXu0gcgmicXnMEYxDwQ30SGZ5AcGJGblDWC5PVQBxdAPteZDQM8AEuaqFU20&currency=PHP"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
		  </script>
			<script>

				console.log(document.querySelector('#total-amount').innerHTML)
				paypal.Buttons({
					createOrder: function(data, actions) {
					// This function sets up the details of the transaction, including the amount and line item details.
					return actions.order.create({
						purchase_units: [{
							amount: {
								value: document.querySelector('#total-amount').innerHTML
							}
						}]
					});
					},
					onApprove: function(data, actions) {
					// This function captures the funds from the transaction.
						return actions.order.capture().then(function(details) {
						// This function shows a transaction success message to your buyer.

						// console.log(data.orderID)
						// alert('Transaction completed by ' + details.payer.name.given_name);
						let formData = new FormData;

						formData.append("orderID",data.orderID);

						option = {
							method:"POST",
							body: formData
						}

						fetch("http://localhost:8000/controllers/create_paypal_transaction.php",option)
						.then( ( response )=>{
							return response.text();
						} )
						.then( ( data )=>{
							console.log(data);
						} )

						});
					}
				}).render('#paypal-smart-button');
  					//This function displays Smart Payment Buttons on your web page.
			</script>
		<?php
	}

 ?>