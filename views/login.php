<?php 
	
	require_once '../partials/template.php';
	
	function get_content(){
		?>
			<div class="justify-content-center align-items-center login-section">
				<div class="row p-5">
					<div class="col-md-4 offset-md-4">
						
					<!-- insert input form for users login page -->
						<form action="./../controllers/authenticate.php" class="" method="POST">
							<div class="icon"><img src="./../assets/images/login.png" alt=""></div>
							<div class="form-section">
								<div class="form-group">
									<label for="email"><i class="fas fa-user-circle text-white mr-2"></i>Enter Email Address:</label>
									<input type="text" name="email"  id="email" class="form-control">
								</div>
								<div class="form-group">
									<label for="password"><i class="fas fa-lock text-white mr-2"></i>Enter Password:</label>
									<input type="password" name="password"  id="password" class="form-control">
								</div>
								<button type="submit" class="btn btn-primary w-100">Login</button>
							</div>
						</form>
					</div>
				</div>		
			</div>
		<?php
	}	
 ?>