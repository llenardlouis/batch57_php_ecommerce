<?php 
	
	require_once '../partials/template.php';

	function get_content(){
		?>
			<?php require_once "./../controllers/connection.php" ?>

			<!-- Create a filter of categories -->
			<ul class="nav categories">
				<!-- create a button that will show all products in the catalog -->
				<li class="nav-item">
					<a href="./catalog.php" class="nav-link">Show All</a>
				</li>
				<!-- create a button for each category for the prducts in the catalog -->
				<?php 
					$category_query = "SELECT * FROM categories";

					$categories = mysqli_query($conn, $category_query);

					foreach ($categories as $indiv_category) {
						?>
							<li class="nav-item">
								<a href="./catalog.php?category_id=<?= $indiv_category['id'] ?>" class="nav-link">
									<?= $indiv_category['name']?>
								</a>
							</li>
						<?php
					}
				 ?>
			</ul>
			<div class="container justify-content-center align-items-center text-center mb-5">				
				<div class="row">
						<!-- pull all the products from the database -->
						<?php 

							$sql_query = "SELECT * FROM products";
							if( isset($_GET['category_id']) ){
								$sql_query .= " WHERE category_id=" . $_GET['category_id'] ;
							}

							// var_dump($_GET['category_id']);
							$products = mysqli_query($conn,$sql_query);

							foreach ($products as $indiv_products) {
								# code...
								// echo $indiv_products['id'];
								?>
									<!-- structure for the cards -->
									<div class="col-sm-3 py-2">
										<div class="card product-list catalog-img">
											<div class="product-image">
												<img src="./../assets/images/<?= $indiv_products['image'] ?>" alt="image unavailable" class="card-img">
											</div>
											<div class="card-body">
												<h4 class="card-title">
													<?= $indiv_products['name'] ?>
												</h4>
												<section class="card-text">
													<?= $indiv_products['description'] ?>
													
													<p class="price">PHP <?= number_format($indiv_products['price'],2); ?></p>
												</section>
											</div>
											<div class="card-footer">
												<!-- quatity of the item that will be ordered by the user -->
												<div class="input-group">
												  <input type="number" class="form-control" value=1>
											
												    <!-- <button class="btn btn-outline-secondary" type="button" id="button-addon2">Button</button> -->
												    <button type="button" class="btn btn-success addToCart input-group-append" data-id="<?= $indiv_products['id'] ?>">Add to Cart</button>
												 
												</div>
												<!-- create admin function -->
												<div class="row">
													<div class="col-12 mx-auto col-sm-8 mb-2">
														<!-- update -->
														<!-- we need to get the specific product id of the selected item -->
														<a href="./edit_product.php?id=<?php echo $indiv_products['id']; ?>" class="btn btn-warning my-1 w-100">Edit Product</a>

														<!-- delete button -->
														<button type="button" data-toggle="modal" data-target="#modal<?php echo $indiv_products['id'] ?>" data-id="<?php echo $indiv_products['id']; ?>" class="bt btn-sm btn-danger my-1 w-100">
															Delete Button
														</button>
													</div>
													<!-- beginning modal -->
													<div class="modal fade" id="modal<?php echo $indiv_products['id'] ?>" tabindex="-1" role="dialog" aria-labelleby="exampleModalLabel" aria-hidden="true"> 
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<!-- modal header -->
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Warning Message</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<!-- modal body -->
																<div class="modal-body">
																	<!-- confirmation message -->
																	Are you sure want to delete this product?
																</div>
																<!-- modal footer -->
																<div class="modal-footer">
																	<a href="./../controllers/delete_product.php?id=<?php echo $indiv_products['id']; ?>" id="confirm-delete" class="btn btn-outline-primary">Yes</a>
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
																</div>
															</div>
														</div>
													</div>
													<!-- end of modal -->
												</div>					
											</div>

										</div>
									</div>
								<?php
							}
						 ?>
				</div>
				<!-- end of product card -->
			</div>
		<?php
	}

 ?>

 <script type="text/javascript" src="./../assets/js/addToCart.js"></script>