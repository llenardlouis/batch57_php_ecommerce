<?php 
	require_once '../partials/template.php';
	// require './../controllers/connection.php';

	function get_content(){
			$firstname = "";
			$lastname = "";
			$email = "";
			$password = "";
			$confirm_password = "";
		?>
			<h3 class="text-center mb-2">Registration Form</h3>
			<div class="container">
				<div class="row">
					<div class="col-md-8 offset-md-2">
						<form action="./../controllers/process_register.php" method="POST" id="registrationForm" class="my-3">
							<div class="row">
								<div class="col-md-6 mx-auto">
									<div class="form-group">
										<label for="firstname">Firstname:</label>
										<input type="text" name="firstname" id="firstname" class="form-control">						
									</div>
									<div class="form-group">
										<label for="lastname">Lastname:</label>
										<input type="text" name="lastname" id="lastname" class="form-control" >
									</div>
									<div class="form-group">
										<label for="email">Email Address:</label>
										<input type="email" name="email" id="email" class="form-control">
									</div>
									<div class="form-group">
										<label for="password">Password:</label>
										<input type="password" name="password" id="password" class="form-control">
									</div>
									<div class="form-group">
										<label for="confirm_password">Confirm Password:</label>
										<input type="password" name="confirm_password" id="confirm_password" class="form-control">
									</div>
									<button id="regBtn" type="submit" class="btn btn-success w-100" name="reg_user">Register</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		<?php
	}

 ?>