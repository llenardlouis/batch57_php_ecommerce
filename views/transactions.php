<?php 

	//invoke the site template
	require_once './../partials/template.php';

	function get_content(){
		?>
			<?php 
				$get_transactions = "SELECT 
				transactions.transaction_code, 
				transactions.total, 
				users.firstname,
				users.lastname, 
				payment_modes.name as payment_name, 
				statuses.name as status_name, 
				transactions.id 
				FROM transactions 
				JOIN users ON (transactions.user_id = users.id) 
				JOIN payment_modes ON (transactions.payment_mode_id = payment_modes.id) 
				JOIN statuses ON (transactions.status_id = statuses.id) 
				WHERE users.id = {$_SESSION['user']['id']} ";
			?>	
			<div class="container">
				<div class="row my-5">
					<div class="col-12">
						<h3 class="text-center">Transaction</h3>
						<!-- invoke the database -->
						<?php
							require_once './../controllers/connection.php';

							//connect the query and the database
							$result = mysqli_query($conn,$get_transactions);
							while($transaction = mysqli_fetch_assoc($result)){

						?>
						<!-- start of transaction details -->
						<div class="table-responsive">
							<table class="table table-hover">
								<!-- transaction code -->
								<tr>
									<td> Transaction Code:</td>
									<td> <?php echo $transaction['transaction_code']; ?></td>
								</tr>
								<!-- customer -->
								<tr>
									<td>Customer Name: </td>
									<td> <?php echo $transaction['firstname']. " " . $transaction['lastname']; ?></td>
								</tr>
								<!-- payment mode -->
								<tr>
									<td>Payment Mode: </td>
									<td> <?php echo $transaction['payment_name']; ?></td>
								</tr>
								<!-- status -->
								<tr>
									<td>Status: </td>
									<td><?php echo $transaction['status_name']; ?></td>
								</tr>
							</table>
							<!-- Purchased Items on transactions -->
							<table class="table table-hover">
								<tbody>
									<tr>
										<td>Product Name: </td>
										<td>Price per Unit: </td>
										<td>Quantity: </td>
										<td>Subtotal</td>
									</tr>
									<?php
										$get_product_transactions = "SELECT 
											product_transactions.quantity,
											products.name,
											products.price FROM product_transactions 
											JOIN products ON (products.id = product_transactions.product_id) 
											WHERE product_transactions.transaction_id = {$transaction['id']}"; 

											//echo $get transaction
											$result_products = mysqli_query($conn,$get_product_transactions);
											while ($product = mysqli_fetch_assoc($result_products)) {
												?>
													<!-- start of row per item -->
													<tr>
														<td><?php echo $product['name']; ?></td>
														<td>&#8369; <?php echo number_format($product['price'],2); ?></td>
														<td><?php echo $product['quantity']; ?></td>
														<td>&#8369;
															<?php echo number_format($product['price'] * $product['quantity'],2); ?>
														</td>
													</tr>
													<!-- end of row per item -->
												<?php
											} 
									?>
					
								</tbody>
								<tfoot>
									<tr class="text-white bg-secondary">
										<td colspan="3">Total:</td>
										<td> &#8369; <?php echo number_format($transaction['total'],2); ?></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- end of transaction table -->
					<hr>
					<?php } ?>
				</div>
			</div>
		<?php
	}

?>