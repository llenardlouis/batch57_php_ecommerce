//add to cart functionality

let addToCartBtns = document.querySelectorAll(".addToCart"); 
//return a collection of buttons has the class name to addToCart

addToCartBtns.forEach(

		function(addToCartBtn){

			addToCartBtn.addEventListener("click",(indiv_button) =>{

				let product_id = indiv_button.target.getAttribute("data-id");

				let product_quantity = indiv_button.target.previousElementSibling.value;

				// create a valdiation to make sure that the quantity is greater than 0
				if(product_quantity <= 0){
					alert("Please enter a Valid Quantity");
				} else {
					//now we will prepare data to be sent via fetc message
					let data = new FormData; //currently an empty object
					data.append("productId", product_id);
					data.append("productQuantity", product_quantity);

					//fetch - is used when sending network request to the server and load new info when needed
					fetch("../controllers/update_cart.php", {
						method: "POST",
						body: data
					})
					.then(function(response){
						return response.text();
					})
					.then(function(data){
						document.querySelector("#cart-count").innerHTML = data;
						location.reload()
					})
				}
			})
		}
	)